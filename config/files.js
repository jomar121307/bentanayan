module.exports.files = {
  zcommerce : {
    productCategories : "protected/product_categories.json"
  },
  petgago : {
    "petTypes" : "protected/pet_types.json"
  },
  countries : "protected/countries.json",
  currencies : "protected/currencies.json",
}
