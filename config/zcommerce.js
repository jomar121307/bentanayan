module.exports.zcommerce = {
  minPrice : 0.01,
  defaultCurrency : "PHP",

  platformFee : {
    percentage : 1,
    flat : 0.3,
    currency : "PHP"
  },

  admin : {
    firstName : "Bentanayan",
    lastName : "Admin",
    username : "bentanayanAdmin",
    password : "p@ssw0rd123!!",
    confirmPassword : "p@ssw0rd123!!",
    email : "admin@shop.zoogtech.com",
    registrationType : "native",
    isAdmin : true,
    appOrigin : "zcommerce"
  },

  appName : "Bentayanan",

  paypalPaymentMemo : "Bentanayan payment",

  mailer : process.env.MAILER || 'noreply@bentanayan.com',

  forgotPasswordRedirect : process.env.FORGOT_PASSWORD_REDIRECT || "http://localhost:1337/#"
}
