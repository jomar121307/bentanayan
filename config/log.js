/**
 * Built-in Log Configuration
 * (sails.config.log)
 *
 * Configure the log level for your app, as well as the transport
 * (Underneath the covers, Sails uses Winston for logging, which
 * allows for some pretty neat custom transports/adapters for log messages)
 *
 * For more information on the Sails logger, check out:
 * http://sailsjs.org/#!/documentation/concepts/Logging
 */

var path = require("path");
var pkgJSON = require(path.resolve("package.json"));

module.exports.log = {

  /***************************************************************************
  *                                                                          *
  * Valid `level` configs: i.e. the minimum log level to capture with        *
  * sails.log.*()                                                            *
  *                                                                          *
  * The order of precedence for log levels from lowest to highest is:        *
  * silly, verbose, info, debug, warn, error                                 *
  *                                                                          *
  * You may also set the level to "silent" to suppress all logs.             *
  *                                                                          *
  ***************************************************************************/

  level: process.env.SAILS_LOG_LEVEL || "info",
  timestamp : true,

  transports : (process.env.NODE_ENV === "production") ? [{
    module: require('winston-slack-webhook').SlackWebHook,
    config: {
      level : "verbose",
      webhookUrl : "https://hooks.slack.com/services/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",
      channel : "#bunyan",
      username : "Bentanayan Bot"
  }}] : null
};
