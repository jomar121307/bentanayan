/**
* ProductReviews.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema : true,

  attributes: {
    productId : {
      model : "products",
      required : true
    },

    userId : {
      model : "users",
      required : true
    },

    content : {
      type : "string",
      maxLength : 1000,
      required : true
    },

    isReply : {
      type : "boolean",
      defaultsTo : false
    },

    reviewId : {
      model : "reviews"
    },

    metadata : {
      type : "json"
    },

    rating : {
      type : "float",
      max : 5,
      defaultsTo : 0
    },

    appType : {
      type : "string",
      enum : sails.config.appList,
      required : true
    },

    isDeleted : {
      type : "boolean",
      defaultsTo : false
    },

    sanitize : function () {
      var review = this;

      modelSanitizer(review);

      delete review.appType;
      delete review.isDeleted;

      return review;
    }
  },

  beforeCreate : function (values, callback, isUpdate) {
    async.series([
      function (seriesCb) {
        ValidationService.global.reviews(values, seriesCb, isUpdate);
      },
      function (seriesCb) {
        ValidationService[values.appType].reviews(values, seriesCb, isUpdate);
      },
      function (seriesCb) {
        ModelHookService.global.reviews(values, seriesCb, isUpdate);
      }
    ], function (err) {
      if (err) {
        return callback(err);
      }

      callback();
    });
  },

  beforeUpdate : function (values, callback) {
    this.beforeCreate(values, callback, true);
  }
};
