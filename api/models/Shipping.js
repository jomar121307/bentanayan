/**
* Address.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema : true,

  attributes: {
    userId : {
      model : "users",
      required : true
    },

    fullName : {
      type : "string",
      required : true
    },

    address : {
      type : "json",
      required : true
    },

    stringifiedAddress : {
      type : "string",
      required : true
    },

    appType : {
      type : "string",
      enum : sails.config.appList,
      required : true
    },

    isDeleted : {
      type : "boolean",
      defaultsTo : false
    },

    sanitize : function () {
      var address = this.toObject();

      delete address.appType;
      delete address.isDeleted;

      return address;
    }
  }
};

