/**
* Notifications.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema : true,

  attributes: {
    entity : {
      type : "string",
      required : true
    },

    entityType : {
      type : "string",
      enum : ["page", "post", "product", "order", "commission", "message"],
      required : true
    },

    notifiedId : {
      model : "users",
      required : true
    },

    notifierId : {
      model : "users",
      required : true
    },

    action : {
      type : "string",
      enum : ["post",
        "follow",
        "buy",
        "place order",
        "receiveCommission",
        "like",
        "comment",
        "comment reply",
        "share",
        "review",
        "rate",
        "feeback",
        "message"],
      required : true
    },

    status : {
      type : "string",
      enum : ["read", "unread"],
      defaultsTo : "unread"
    },

    metadata : {
      type : "json"
    },

    appType : {
      type : "string",
      enum : sails.config.appList,
      required : true
    },

    isDeleted : {
      type : "boolean",
      defaultsTo : false
    }
  }
};

