/**
* Orders.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema : true,

  attributes: {
    //
    // General Details
    //
    userId : {
      model : "users",
      required : true
    },

    shopId : {
      model : "shops",
      required : true
    },

    referenceId : {
      type : "string",
      required : true
    },

    transactionId : {
      model : "transactions",
      required : true
    },

    payKey : {
      type : "string",
      required : true
    },

    paymentPlatform : {
      type : "string",
      enum : sails.config.paymentPlatformList
    },

    shippingId : {
      model : "shipping",
      required : true
    },

    items : {
      type : "array",
      required : true
    },

    status : {
      type : "string",
      enum : [
        "pending",
        "on hold",
        "shipped",
        "received",
        "refunded"
      ],
      defaultsTo : "pending"
    },

    appType : {
      type : "string",
      enum : sails.config.appList,
      required : true
    },

    isCompleted : {
      type : "boolean",
      defaultsTo : false
    },

    isDeleted : {
      type : "boolean",
      defaultsTo : false
    },

    sanitize : function () {
      var order = this.toObject();

      delete order.transactionId;
      delete order.appType;
      delete order.isCompleted;
      delete order.isDeleted;

      return order;
    }
  }
};

