/**
* Sessions.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema : true,

  attributes: {
    userId : {
      model : "users",
      required : true
    },

    refreshToken : {
      type : "string",
      required : true
    },

    accessToken : {
      type : "string",
      required : true
    },

    expiresAt : {
      type : "date",
      defaultsTo : new Date(Date.now() + sails.config.defTokenExpires)
    },

    appType : {
      type : "string",
      enum : sails.config.appList,
      required : true
    },

    metadata : {
      type : "json"
    },

    isDeleted : {
      type : "boolean",
      defaultsTo : false
    },

    sanitize : function () {
      var session = this.toObject();

      return {
        userId : session.userId,
        refreshToken : session.refreshToken,
        accessToken : session.accessToken
      }
    },

    loginPayload : function (options, callback) {
      var session = this.sanitize();

      sails.log.verbose("Generating login payload for user", session.userId);

      if (!Utils.checkProps(options, ["appType", "userId"])) {
       return callback({
          type : "serverError",
          msg : ""
        })
      }

      async.waterfall([
        function (waterfallCb) {
          Users.findOneById(session.userId)
            .exec(function (err, foundUser) {
              if (err) {
                return waterfallCb(err);
              } else if (!foundUser) {
                sails.log.error("Sessons::loginPayload::User not found.");
                return waterfallCb({
                  type : "serverError",
                  msg : ""
                });
              }

              session.userId = foundUser.profileSanitize();

              waterfallCb(null, {
                session : session,
                shop : null
              });
            });
        },
        function (payload, waterfallCb) {
          Shops.findOne({
            userId : options.userId,
            isDeleted : false
          }).exec(function (err, foundShop) {
            if (err) {
              return waterfallCb(err);
            } else if (!foundShop) {
              sails.log.error("User shop not found.", options.userId);
              return waterfallCb({
                type : "serverError",
                msg : ""
              });
            }

            payload.shop = foundShop.id;
            waterfallCb(null, payload);
          });
        }
      ], function (err, waterfallResult) {
        if (err) {
          return callback(err);
        }

        callback(null, waterfallResult);
      })
    }
  }
};

