/**
* Transactions.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema : true,

  attributes: {
    userId : {
      model : "users",
      required : true
    },

    receivers : {
      type : "json",
      required : true
    },

    payKey : {
      type : "string",
      required : true
    },

    payment : {
      type : "json",
    },

    paymentBreakdown : {
      type : "json",
      required : true
    },

    paymentPlatform : {
      type : "string",
      enum : sails.config.paymentPlatformList
    },

    status : {
      type : "string",
      enum : ["pending", "complete", "abandoned"],
      defaultsTo : "pending"
    },

    appType : {
      type : "string",
      enum : sails.config.appList,
      required : true
    },

    isDeleted : {
      type : "boolean",
      defaultsTo : false
    }
  }
};
