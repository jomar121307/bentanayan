/**
* ProductBrands.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema : true,

  attributes: {
    name : {
      type : "string",
      required : true
    },

    productCount : {
      type : "integer",
      defaultsTo : 0
    },

    metadata : {
      type : "json",
      defaultsTo : {}
    },

    appType : {
      type : "string",
      enum : sails.config.appList,
      required : true
    },

    isDeleted : {
      type : "boolean",
      defaultsTo : false
    },

    sanitize : function () {
      var brand = this.toObject();
      return _.pick(brand, ["name", "productCount", "metadata"]);
    }
  }
};

