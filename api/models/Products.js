/**
* Products.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var ObjectID = require("objectid");
var underscore_string = require("underscore.string");

module.exports = {
  schema : true,

  types : {
    updateReadOnly : function () {
      return true;
    }
  },

  attributes: {
    userId : {
      model : "users",
      required : true,
      updateReadOnly : true
    },

    shopId : {
      model : "shops",
      required : true,
      // updateReadOnly : true checked in product validations
    },

    category : {
      type : "string",
      required : true
    },

    brand : {
      type : "string",
      maxLength : 50,
      required : true
    },

    name : {
      type : "string",
      maxLength : 100,
      required : true
    },

    description : {
      type : "string",
      maxLength : 500,
      required : true
    },

    quantity : {
      type : "integer",
      min : 0,
      max : 9999,
      required : true
    },

    price : {
      type : "float",
      required : true
    },

    metadata : {
      type : "json",
      defaultsTo : {}
    },

    reviewScore : {
      type : "float",
      defaultsTo : 0,
      updateReadOnly : true
    },

    reviewCount : {
      type : "integer",
      defaultsTo : 0,
      updateReadOnly : true
    },

    currencyId : {
      model : "currencies",
      updateReadOnly : true
    },

    randomPoint : {
      type : "array",
      defaultsTo : [Math.random(), Math.random()],
      updateReadOnly : true
    },

    appType : {
      type : "string",
      enum : sails.config.appList,
      required : true,
      updateReadOnly : true
    },

    isDeleted : {
      type : "boolean",
      defaultsTo : false,
      updateReadOnly : true
    },

    sanitize : function (appType) {
      var product = this;
      modelSanitizer(product);
      product = product.toObject();

      delete product.randomPoint;
      delete product.appType;
      delete product.isDeleted;

      return product;
    },

    /**
     * Checks if the product is already resold by you.
     *
     * @method     checkResellStatus
     * @param      {Function}  callback  { description }
     */
    checkResellStatus : function (callback) {
      var product = this;

      Products.native(function (err, collection) {
        if (err) {
          return callback(err);
        }

        collection.findOne({
          userId : product.userId,
          "metadata.sourceProductId" : product.id,
          "metadata.isResold" : true,
          appType : product.appType,
          isDeleted : false
        }, function (err, foundProduct) {
          if (err) {
            return callback(err);
          } else if (foundProduct) {
            callback({
              type : "badRequest",
              msg : "You are already selling this product."
            });
          }

          callback();
        });
      });
    },

    checkInCart : function (options, callback) {
      var product = this.toObject();

      return product;
    },

    checkIfReviewed : function (options, callback) {
      var product = this.toObject();

      return product;
    },

    /**
     * Fetch reviews of a product.
     *
     * @param {Object} options
     * @param {String} options.appType
     * @param {Object} [options.where]
     * @param {Number} [options.skip]
     * @param {Number} options.limit
     * @param {String} [options.sort]
     * @param {Function} callback
     *
     */
    fetchReviews : function (options, callback) {
      var product = this;

      if (typeof callback !== "function") {
        throw new Error("Callback is not a function.");
      } else if (!Utils.checkProps(options, ["appType", "limit"])) {
        sails.log.error("Products :: fetchReviews :: Missing parameters.");
        return callback({
          type : "serverError",
          msg : ""
        });
      }

      try {
        if (options.where) {
          options.where = JSON.parse(options.where);
        } else {
          options.where = {};
        }
      } catch (e) {
        sails.log.error(e);
        return callback({
          type : "badRequest",
          msg : "Malformed query"
        });
      }

      Reviews.find({
        productId : product.id,
        appType : options.appType,
        isDeleted : false
      }).limit(options.limit)
        .skip(options.skip)
        .sort(options.sort)
        .where(options.where)
        .populate("userId")
        .exec(function (err, foundReviews) {
          if (err) {
            return callback(err);
          }

          product.reviews = _.map(foundReviews, function (review) {
            return review.sanitize();
          });
          callback();
        });
    }
  },

  beforeCreate : function (values, callback, isUpdate) {
    async.series([
      function (seriesCb) {
        ValidationService.global.products(values, seriesCb, isUpdate);
      },
      function (seriesCb) {
        ValidationService[values.appType].products(values, seriesCb, isUpdate);
      },
      function (seriesCb) {
        ModelHookService.global.products(values, seriesCb, isUpdate);
      }
    ], function (err) {
      if (err) {
        return callback(err);
      }

      callback();
    });
  },

  beforeUpdate : function (values, callback) {
    this.beforeCreate(values, callback, true);
  },

  /**
   * Find random products.
   *
   * @param {String} options.appType
   * @param {Object} options.where
   * @param {Number} [options.skip]
   * @param {Number} [options.limit]
   * @param {String} [options.sort]
   * @param {Function} nodejs callback
   * @returns {Object} Cursor
   * @returns {Number} Cursor.count
   * @returns {Array} Cursor.products
   *
   */
  findRandom : function (options, callback) {
    if (typeof callback !== "function") {
      throw new Error("Callback is not a function.");
    } else if (!Utils.checkProps(options, ["appType", "where"])) {
      sails.log.error("Shops :: findRandom :: Missing parameters.");
      return callback({
        type : "serverError",
        msg : ""
      });
    }

    try {
      if (options.where) {
        options.where = JSON.parse(options.where);
      } else {
        options.where = {};
      }

      if (typeof options.sort === "string" && _.startsWith(options.sort, "{")) {
        options.sort = JSON.parse(options.sort);
       } else {
        _.each(options.sort, function (v, k) {
          options.sort[k] = parseInt(v);
        });
       }

    } catch (e) {
      sails.log.error(e);
      return callback({
        type : "badRequest",
        msg : "Malformed query"
      });
    }

    if (!options.where.xRandom || !options.where.yRandom) {
      return callback({
        type : "badRequest",
        msg : "Lacking random points. Add xRandom and yRandom to where with values [0,1)."
      });
    }

    async.waterfall([
      function (waterfallCb) {
        var x = options.where.xRandom;
        var y = options.where.yRandom;
        var countQuery = {
          appType : options.appType,
          isDeleted : false
        };
        var findQuery = {
          randomPoint : {
            $near : [x, y]
          }
        }

        delete options.where.xRandom;
        delete options.where.yRandom;

        countQuery = _.merge({}, countQuery, options.where);
        findQuery = _.merge({}, findQuery, countQuery);

        Products.native(function (err, collection) {
          if (err) {
            return waterfallCb(err);
          }

          collection.count(countQuery,
            function (err, count) {
              if (err) {
                return waterfallCb(err);
              } else if (!count) {
                return waterfallCb(null, {
                  count : 0,
                  products : []
                });
              }

              collection.find(findQuery, {
                skip : options.skip,
                limit : options.limit,
                sort : options.sort
              }).toArray(function (err, foundProduct) {
                if (err) {
                  return waterfallCb(err);
                }

                waterfallCb(null, {
                  count : count,
                  products : foundProduct
                });
              });
            });
        });
      },
      function (payload, waterfallCb) {
        async.map(payload.products, function (product, mapCb) {
          Products.findOne({
            id : product._id + "",
            isDeleted : false
          }).populate("userId")
            .exec(function (err, foundProduct) {
              if (err) {
                return mapCb(err);
              } else if (!foundProduct) {
                sails.log.info("Products :: findRandom :: Product not found; maybe deleted.", product._id);
                return mapCb();
              }

              mapCb(null, foundProduct);
            });
        }, function (err, mapResult) {
          if (err) {
            return waterfallCb(err);
          }

          payload.products = _.compact(mapResult);
          waterfallCb(null, payload);
        });
      }
    ], function (err, waterfallResult) {
      if (err) {
        return callback(err);
      }

      callback(null, waterfallResult);
    });
  }
};
