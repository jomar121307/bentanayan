module.exports = require('include-all')({
  dirname     :  __dirname + '/modelhooks',
  filter      :  /(.+)\.js$/,
  excludeDirs :  /^\.(git|svn)$/,
  optional    :  true
});

