module.exports = function (values, callback, isUpdate) {
  sails.log.verbose("Running Validation :: Z-Commerce :: Users");
  sails.log.verbose(values);

  if (values.appOrigin !== "zcommerce") {
    return process.nextTick(callback);
  } else if (isUpdate) {
    return Currencies.findOne({
      code : "PHP"
    }).exec(function (err, foundCurrency) {
      if (err) {
        return callback(err);
      } else if (!foundCurrency) {
        sails.log.error("Validation :: Z-Commerce :: Users :: PHP currency not found.");
        return callback({
          type : "serverError",
          msg : ""
        });
      }

      values.currencyId = foundCurrency.id;
      callback();
    });
  }

  Currencies.findOne({
    code : "PHP"
  }).exec(function (err, foundCurrency) {
    if (err) {
      return callback(err);
    } else if (!foundCurrency) {
      sails.log.error("Validation :: Z-Commerce :: Users :: PHP currency not found.");
      return callback({
        type : "serverError",
        msg : ""
      });
    }

    values.currencyId = foundCurrency.id;
    callback();
  });
}
