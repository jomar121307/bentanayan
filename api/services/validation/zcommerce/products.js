module.exports = function (values, callback, isUpdate) {
  sails.log.verbose("Running Validation :: Z-Commerce :: Products");
  sails.log.verbose(values);

  if (!values.appType) {
    throw new Error("Validations :: Z-Commerce :: Products :: App type is undefined.");
  } else if (isUpdate) {
    return async.series([
      function (seriesCb) {
        checkPrice(values, seriesCb);
      }
    ], function (err) {
      if (err) {
        return callback(err);
      }

      callback();
    });
  }

  async.series([
    function (seriesCb) {
      checkPrice(values, seriesCb);
    }
  ], function (err) {
    if (err) {
      return callback(err);
    }

    callback();
  });
};

/**
 * Check price.
 *
 * @method     checkPrice
 * @param      {object}    values    { description }
 * @param      {Function}  callback  { description }
 */
function checkPrice(values, callback) {
  if (values.price < sails.config.zcommerce.minPrice) {
    return callback(ErrorService.validation({
      price : [
        {
          rule : "valid",
          message : "Price should be greater than " + sails.config.zcommerce.minPrice
        }
      ]
    }));
  }

  callback();
}

