module.exports = function (values, callback, isUpdate) {
  sails.log.verbose("Running Validation :: Global :: Products :: " + (isUpdate ? "onUpdate" : "onCreate"));
  sails.log.verbose(values);

  if (!values.appType) {
    throw new Error("Validations :: Global :: Products :: App type is undefined.");
  } else if (isUpdate) {
    return async.series([
      function (seriesCb) {
        checkName(values, seriesCb);
      },
      function (seriesCb) {
        checkBrand(values, seriesCb);
      },
      // function (seriesCb) {
      //   checkCategory(values, seriesCb);
      // }
    ], function (err) {
      if (err) {
        return callback(err);
      }

      delete values.__old;
      callback();
    });
  }

  async.series([
    function (seriesCb) {
      checkShop(values, seriesCb);
    },
    function (seriesCb) {
      checkName(values, seriesCb);
    },
    function (seriesCb) {
      addBrand(values, seriesCb);
    },
    function (seriesCb) {
      checkCategory(values, seriesCb);
    },
    function (seriesCb) {
      checkMetadata(values, seriesCb);
    },
  ], callback);
};

/**
 * Check if shop is valid and add currencyId and productCode for product.
 *
 * @method     checkShop
 * @param      {Object}    values    { description }
 * @param      {Function}  callback  { description }
 */
function checkShop (values, callback) {
  Shops.findOne({
    id : values.shopId,
    userId : values.userId,
    isDeleted : false
  }).exec(function (err, foundShop) {
    if (err) {
      return callback(err);
    } else if (!foundShop) {
      return callback(ErrorService.validation({
        shopId : [
          {
            rule : "valid",
            message : "Shop not found."
          }
        ]
      }));
    }

    values.metadata.productCode = foundShop.id + '-' +
      foundShop.metadata[values.appType].productCount;
    values.currencyId = foundShop.metadata[values.appType].currencyId;
    callback();
  });
};

/**
 * Checks product name for in-store duplicates.
 *
 * @method     checkName
 * @param      {Object}    values    { description }
 * @param      {Function}  callback  { description }
 */
function checkName (values, callback) {
  Products.findOne({
    userId : values.userId,
    shopid : values.shopid,
    name : values.name,
    appType : values.appType,
    isDeleted : false
  }).exec(function (err, foundProduct) {
    if (err) {
      return callback(err);
    } else if (foundProduct && foundProduct.id === values.id) {
      return callback();
    } else if (foundProduct) {
      return callback(ErrorService.validation({
        name : [
          {
            rule : "Duplicate",
            message : "You already have a product named " +
            values.name + "."
          }
        ]
      }));
    }

    callback();
  });
}

/**
 * Automatically adds a brand name if none is found.
 *
 * @method     addBrand
 * @param      {Object}    values    { description }
 * @param      {Function}  callback  { description }
 */
function addBrand (values, callback) {
  Brands.native(function (err, collection) {
    if (err) {
      return callback(err);
    }

    collection.findOneAndUpdate({
      name : values.brand,
      appType : values.appType,
      isDeleted : false
    }, {
      $inc : {
        productCount : 1
      }
    }, function (err, result) {
      if (err) {
        return callback(err);
      } else if (!result.value) {
        return Brands.create({
          name : values.brand,
          appType : values.appType
        }).exec(function (err, createdBrand) {
          if (err) {
            return callback(err);
          }

          callback();
        });
      }

      callback();
    });
  });
}

/**
 * Checks the brand name for updating products.
 *
 * @method     checkBrand
 * @param      {Object}    values    { description }
 * @param      {Function}  callback  { description }
 */
function checkBrand (values, callback) {
  _.ensure(values, "__old.brand", "");
  if (!values.__old || values.brand === values.__old.brand) {
    return process.nextTick(callback);
  }

  addBrand(values, callback);
}

/**
 * Checks for miscellaneous properties like photos.
 *
 * @method     checkMetadata
 * @param      {Object}    values    { description }
 * @param      {Function}  callback  { description }
 */
function checkMetadata (values, callback) {
  var idx = -1;

  _.ensure(values, "metadata.photos", []);

  if (!values.metadata.photos.length) {
    return callback(ErrorService.validation({
      metadata : [
        {
          rule : "required",
          message : "Photos must be a cloudinary object array (object has secure_url as media url)"
        }
      ]
    }));
  }

  idx = _.findIndex(values.metadata.photos, function (photoObj) {
    if (!photoObj.secure_url || typeof photoObj.secure_url !== "string") {
      return true;
    }

    return photoObj.secure_url.indexOf("https://res.cloudinary.com") === -1;
  });

  if (idx !== -1) {
    return callback(ErrorService.validation({
      metadata : [
        {
          rule : "valid",
          message : "metadata.photos[" + idx + "] is not a valid cloudinary obj."
        }
      ]
    }));
  }

  callback();
}

/**
 * Checks if the product category conforms to the specific application
 *
 * @method     checkCategory
 * @param      {Object}    values    { description }
 * @param      {Function}  callback  { description }
 */
function checkCategory (values, callback) {
  if (!values.category) {
    return callback(ErrorService.validation({
      metadata : [
        {
          rule : "required",
          message : "Product category is undefined."
        }
      ]
    }));
  }

  var splat = values.category.split(".");
  Misc.findOne({
    name : "Product Categories",
    appType : values.appType
  }).exec(function (err, categories) {
    if (err) {
      return callback(err);
    } else if (!categories) {
      sails.log.error("Validations :: Global :: Products :: Categories missing for ", values.appType);
      return callback("Error.");
    }

    var idx = _.findIndex(categories.metadata, {
      slug : splat[0]
    });
    var subcategory = categories.metadata[idx] || {
      subcategories : []
    };
    var group = _.filter(subcategory.subcategories, function (subcat) {
      return subcat.groupheader.slug === splat[1];
    })[0];

    var grouplingIdx = _.findIndex(_.safe(group, "groupling", []), function (gl) {
      return gl.slug === splat[2];
    });

    if (!_.safe(subcategory, "subcategories.length")) {
      return callback(ErrorService.validation({
        metadata : [
          {
            rule : "valid",
            message : "Product category must be valid."
          }
        ]
      }));
    } else if (!group) {
      return callback(ErrorService.validation({
        metadata : [
          {
            rule : "required",
            message : "Product subcategory must be valid."
          }
        ]
      }));
    } else if (_.safe(group, "groupling.length") && splat.length === 2) {
      return callback(ErrorService.validation({
        metadata : [
          {
            rule : "required",
            message : "Product subgroup is required."
          }
        ]
      }));
    } else if (!_.safe(group, "groupling.length") && splat.length === 3) {
      return callback(ErrorService.validation({
        metadata : [
          {
            rule : "valid",
            message : "Product subgroup is required."
          }
        ]
      }));
    } else if (grouplingIdx === -1 && splat.length === 3) {
      return callback(ErrorService.validation({
        metadata : [
          {
            rule : "valid",
            message : "Product subgroup must be valid."
          }
        ]
      }));
    }

    callback();
  });
}
