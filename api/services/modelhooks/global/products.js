/**
 * This hook assumes the values parameter has been validated.
 *
 * @param {object} values
 * @param {function} nodejs callback
 *
 */

var ObjectID = require("objectid");

module.exports = function (values, callback, isUpdate) {
  sails.log.verbose("Running Hook :: Global :: Products :: " + (isUpdate ? "onUpdate" : "onCreate"));
  sails.log.verbose(values);

  if (!values.appType) {
    throw new Error("Hooks :: Global :: Products :: App type is undefined");
  } else if (isUpdate) {
    return async.series([
      function (seriesCb) {
        decrementProductCount(values, seriesCb);
      }
    ], function (err) {
      if (err) {
        return callback(err);
      }

      callback();
    });
  }

  async.series([
    function (seriesCb) {
      incrementProductCount(values, seriesCb);
    }
  ], function (err) {
    if (err) {
      return callback(err);
    }

    callback();
  });
};

/**
 * Increments the product count of a shop.
 *
 * @method     incrementProductCount
 * @param      {<type>}    values    { description }
 * @param      {Function}  callback  { description }
 */
function incrementProductCount (values, callback) {
  sails.log.verbose("Hooks :: Global :: Products :: incrementProductCount");

  var inc = {};
  var category = values.category.split(".");
  inc["metadata." + values.appType + ".productCount"] = 1

  async.waterfall([
    function (waterfallCb) {
      Shops.native(function (err, collection) {
        if (err) {
          return callback(err);
        }

        waterfallCb(null, collection);
      });
    },
    function (collection, waterfallCb) {
      collection.findOneAndUpdate({
        _id : ObjectID(values.shopId),
        isDeleted : false,
      }, {
        $inc : inc
      }, function (err, result) {
        if (err) {
          return waterfallCb(err);
        }

        waterfallCb();
      });
    },
    function (waterfallCb) {
      Misc.native(function (err, collection) {
        if (err) {
          return waterfallCb(err);
        }

        waterfallCb(null, collection);
      });
    },
    function (collection, waterfallCb) {
      collection.findOneAndUpdate({
        name : "Product Categories",
        appType : values.appType,
        slug : category[0],
        "subcategories.slug" : category[1]
      }, {
        $inc : {
          "subcategories.$.count" : 1
        }
      }, function (err, result) {
        if (err) {
          return callback(err);
        }

        callback();
      });
    }
  ], function (err, waterfallResult) {
    if (err) {
      return callback(err);
    }

    callback();
  });
}

/**
 * Decrements the product count of a shop
 *
 * @method     decrementProductCount
 * @param      {Object}    values    { description }
 * @param      {Function}  callback  { description }
 */
function decrementProductCount (values, callback) {
  if (!values.isDeleted) {
    return process.nextTick(callback);
  }

  sails.log.verbose("Hooks :: Global :: Products :: decrementProductCount");

  var inc = {};
  var category = values.category.split(".");
  inc["metadata." + values.appType + ".productCount"] = -1

  async.waterfall([
    function (waterfallCb) {
      Shops.native(function (err, collection) {
        if (err) {
          return waterfallCb(err);
        }

        waterfallCb(null, collection);
      });
    },
    function (collection, waterfallCb) {
      collection.findOneAndUpdate({
        _id : ObjectID(values.shopId),
        isDeleted : false
      }, {
        $inc : inc
      }, function (err, result) {
        if (err) {
          return waterfallCb(err);
        }

        waterfallCb();
      });
    },
    function (waterfallCb) {
      Misc.native(function (err, collection) {
        if (err) {
          return waterfallCb(err);
        }

        waterfallCb(null, collection);
      });
    },
    function (collection, waterfallCb) {
      collection.findOneAndUpdate({
        name : "Product Categories",
        appType : values.appType,
        slug : category[0],
        "subcategories.slug" : category[1]
      }, {
        $inc : {
          "subcategories.$.count" : -1
        }
      }, function (err, result) {
        if (err) {
          return callback(err);
        }

        callback();
      });
    }
  ], function (err, waterfallResult) {
    if (err) {
      return callback(err);
    }

    callback();
  });
}
