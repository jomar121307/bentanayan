/**
 * This hook assumes the values parameter has been validated.
 *
 * @param {object} values
 * @param {function} nodejs callback
 *
 */

var ObjectID = require("objectid");

module.exports = function (values, callback, isUpdate) {
  sails.log.verbose("Running Hook :: Global :: Reviews");
  sails.log.verbose(values);

  if (!values.appType) {
    throw new Error("Hooks :: Global :: Reviews :: App type is undefined.");
  } else if (isUpdate) {
    return async.series([
      function (seriesCb) {
        decrementReviewCount (values, seriesCb);
      }
    ], function (err) {
      if (err) {
        return callback(err);
      }

      callback();
    });
  }

  async.series([
    function (seriesCb) {
      incrementReviewCount(values, seriesCb);
    }
  ], function (err) {
    if (err) {
      return callback(err);
    }

    callback();
  });
};

/**
 * Increments the review count of a product.
 *
 * @method     incrementReviewCount
 * @param      {Object}    values    { description }
 * @param      {Function}  callback  { description }
 */
function incrementReviewCount (values, callback) {
  Products.native(function (err, collection) {
    if (err) {
      return callback(err);
    }

    collection.findOneAndUpdate({
      _id : ObjectID(values.productId),
      appType : values.appType,
      isDeleted : false
    }, {
      $inc : {
        reviewCount : 1
      }
    }, function (err, result) {
      if (err) {
        return callback(err);
      }

      callback();
    });
  });
}

/**
 * Decrements the review count of a product.
 *
 * @method     decrementReviewCount
 * @param      {Object}    values    { description }
 * @param      {Function}  callback  { description }
 */
function decrementReviewCount (values, callback) {
  if (!values.isDeleted) {
    return process.nextTick(callback);
  }

  Products.native(function (err, collection) {
    if (err) {
      return callback(err);
    }

    collection.findOneAndUpdate({
      _id : ObjectID(values.productId),
      appType : values.appType,
      isDeleted : false
    }, {
      $inc : {
        reviewCount : -1
      }
    }, function (err, result) {
      if (err) {
        return callback(err);
      }

      callback();
    });
  });
}
