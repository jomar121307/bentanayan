/**
 * The hook assumes the values parameter has been validated.
 *
 * @param {object} values
 * @param {function} nodejs callback
 *
 */

module.exports = function (values, callback, isUpdate) {
  sails.log.verbose("Running Hook :: Global :: Users");
  sails.log.verbose(values);

  if (isUpdate) {
    return async.series([
      function (seriesCb) {
        process.nextTick(seriesCb);
      }
    ], function (err) {
      if (err) {
        return callback(err);
      }

      callback();
    });
  }

  async.series([
    function (seriesCb) {
      addShop(values, seriesCb);
    }
  ], function (err) {
    if (err) {
      return callback(err);
    }

    callback();
  });
};

function addShop (values, callback) {
  var obj = {
    userId : values.id,
    appOrigin : values.appOrigin,
    isDeleted : false
  }
  if (values.appOrigin === "zcommerce") {
    _.ensure(obj, "metadata.zcommerce", {});
    sails.log.debug(obj, values);
    obj.metadata.zcommerce.eCashInfo = values.metadata.zcommerce;
  }

  Shops.create(obj).exec(function (err, createdShop) {
    if (err) {
      return callback(err);
    }

    callback();
  });
}

