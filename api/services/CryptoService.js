var sha256 = require("crypto-js/sha256");
var bcrypt = require("bcrypt");
var randomToken = require("random-token").create("0123456789");

module.exports = {
  generateToken : function(numbersOnly) {
    if (numbersOnly) {
      return randomToken(10);
    }

    return sha256(Math.floor(Math.random() * 1000) + "" + new Date().getTime()) + "";
  },

  getHash : function(value, cb) {
    if (_.isFunction(cb)) {
      bcrypt.genSalt(sails.config.saltWorkFactor, function(err, salt) {
        if (err) {
          return cb(err);
        }

        // hash the password using our new salt
        bcrypt.hash(value, salt, function(err, hash) {
          if (err) {
            return cb(err);
          }

          cb(null, hash);
        });
      });
    } else {
      return sha256(value) + "";
    }
  },

  compareHash : function(candidate, hash, cb) {
    sails.log.verbose("Comparing hashes.", candidate, hash);
    bcrypt.compare(candidate, hash, function(err, isMatch) {
      if (err) {
        return cb(err);
      }

      cb(null, isMatch);
    });
  }
};

