/**
 * CurrenciesController
 *
 * @description :: Server-side logic for managing Currencies
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  create : function (req, res) {
    res.notFound();
  },

  update : function (req, res) {
    res.notFound();
  },

  destroy : function (req, res) {
    res.notFound();
  }
};

