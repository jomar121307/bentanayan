/**
 * ChargeController
 *
 * @description :: Server-side logic for managing charges
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  create : function (req, res) {

  },

  find : function (req, res) {
    res.notFound();
  },

  findOne : function (req, res) {
    res.notFound();
  },

  update : function (req, res) {
    res.notFound();
  },

  destroy : function (req, res) {
    res.notFound();
  }
};

