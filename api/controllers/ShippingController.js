/**
 * ShippingController
 *
 * @description :: Server-side logic for managing Shipping
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  create : function (req, res) {
    _.assign(req.body, {
      userId : req.userId,
      fullName : req.body.address.fullName,
      appType : req.appType,
      stringifiedAddress : JSON.stringify(req.body.address)
    });

    Shipping.find({
      userId : req.userId,
      stringifiedAddress : req.body.stringifiedAddress,
      appType : req.appType,
      isDeleted : false
    }).exec(function  (err, address) {
      if (err) {
        return res.serverError(err);
      } else if (address.length) {
        return res.badRequest("Address already added.");
      }
      Shipping.create(req.body).exec(function (err, createdAddress) {
        if (err) {
          return res.serverError(err);
        }
        res.ok(createdAddress.sanitize());
      });
    });
  },

  find : function (req, res) {
    Shipping.find({
      userId : req.userId,
      isDeleted : false
    }).exec(function (err, addresses) {
      if (err) {
        return res.serverError(err);
      }

      addresses = _.map(addresses, function (a) {
        return a.sanitize();
      });
      res.ok(addresses);
    });
  },

  findOne : function (req, res) {
    Shipping.findOne({
      userId : req.userId,
      id : req.params.id,
      isDeleted : false
    }).populateAll()
      .exec(function (err, foundAddress) {
        if (err) {
          return res.serverError(err);
        } else if (!foundAddress) {
          return res.badRequest("Address not found.");
        }

        res.ok(foundAddress);
      });
  },

  update : function (req, res) {
    return res.notFound();
  },

  destroy : function (req, res) {
    Shipping.findOne({
      id : req.params.id,
      userId : req.userId,
      isDeleted : false
    }).exec(function (err, foundAddress) {
      if (err) {
        return res.serverError(err);
      } else if (!foundAddress) {
        return res.badRequest("Address not found.");
      }

      foundAddress.isDeleted = true;
      foundAddress.save(function (err) {
        if (err) {
          return res.serverError(err);
        }

        res.ok({
          status : "success"
        });
      });
    });
  }
};
