/**
 * UsersController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var cryptojs = require("crypto-js");

module.exports = {
	create : function (req, res) {
    _.extend(req.body, {
      registrationType : "native",
      isAdmin : false,
      appOrigin : req.appType
    });

    Users.create(req.body)
      .exec(function (err, createdUser) {
        if (err) {
          return res.serverError(err);
        }

        res.ok(createdUser.sanitize());
      });
  },

  find : function (req, res) {
    if (!req.query.limit || req.query.limit > 100 || req.query.limit < 0) {
      req.query.limit = 100;
    }

    Users.find({
      where : req.query.where,
      skip : req.query.skip,
      limit : req.query.limit,
      sort : req.query.sort,
      isDeleted : false
    }).exec(function (err, foundUsers) {
      if (err) {
        return res.serverError(err);
      }

      async.each(foundUsers, function (user, eachCb) {
        user.checkBlocked({
          userId : req.userId,
          appType : req.appType
        }, function (err) {
          if (err) {
            return eachCb(err);
          } else if (user.id === req.userId) {
            user.__self = true;
          }

          eachCb();
        });
      }, function (err) {
        if (err) {
          if (res.hasOwnProperty(err.type)) {
            return res[err.type](err.msg);
          }

          return res.serverError(err);
        }

        res.ok(foundUsers);
      });
    });
  },

  findOne : function (req, res) {
    var query = {
      id : req.params.id,
      isDeleted : false
    };

    if (req.params.id === "me") {
      query.id = req.userId;
    }

    Users.findOne(query)
      .exec(function (err, foundUser) {
        if (err) {
          return res.serverError(err);
        } else if (!foundUser) {
          return res.notFound("User not found.");
        }

        foundUser.checkBlocked({
          userId : req.userId,
          appType : req.appType
        }, function (err) {
          if (err) {
            if (res.hasOwnProperty(err.type)) {
              return res[err.type](err.msg);
            }

            return res.serverError(err);
          }

          if (foundUser.id === req.userId) {
            foundUser.__self = true;
          }

          res.ok(foundUser.sanitize());
        });
    });
  },

  feed : function (req, res) {
    var query = {
      userId : req.userId,
      appType : req.appType,
      isDeleted : false
    };

    if (!req.query.limit || req.query.limit > 100 || req.query.limit < 0) {
      req.query.limit = 100;
    }

    Feed.count(query)
      .exec(function (err, feedCount) {
        if (err) {
          return res.serverError(err);
        } else if (!feedCount) {
          return res.ok({
            count : 0,
            items : []
          });
        }

        Feed.find({
          userId : req.userId,
          appType : req.appType,
          isDeleted : false
        }).skip(req.query.skip)
          .limit(req.query.limit)
          .sort(req.query.sort)
          .exec(function (err, items) {
          if (err) {
            return res.serverError(err);
          }

          async.map(items, function (item, mapCb) {
            Posts.findOne({
              id : item.postId,
              isDeleted : false
            }).populate("userId")
              .exec(function (err, foundPost) {
              if (err) {
                return mapCb(err);
              } else if (!foundPost) {
                sails.log.error("UsersController :: feed :: Some deleted posts are still lingering in the feeds.");
                return mapCb({
                  type : "serverError",
                  msg : ""
                });
              }

              foundPost.userId = foundPost.userId.sanitize();
              mapCb(null, foundPost.sanitize());
            });
          }, function (err, mapResult) {
            if (err) {
              if (res.hasOwnProperty(err.type)) {
                return res[err.type](err.msg);
              }

              return res.serverError(err);
            }

            res.ok({
              count : feedCount,
              items : mapResult
            });
          });
        });
      });
  },

  update : function (req, res) {
    if (req.params.id !== req.userId) {
      return res.serverError();
    }

    delete req.body.username;
    delete req.body.fbId;
    delete req.body.twitterId;
    delete req.body.googleId;
    delete req.body.notificationCount;
    delete req.body.pageCount;
    delete req.body.registrationType;
    delete req.body.randomPoint;
    delete req.body.isAdmin;
    delete req.body.isDeleted;

    Users.findOneById(req.userId)
      .exec(function (err, foundUser) {
        if (err) {
          return res.serverError(err);
        } else if (!foundUser) {
          return res.notFound("User not found.");
        }

        foundUser.__old = foundUser.toObject();
        _.extend(foundUser, req.body);

        foundUser.save(function (err) {
          if (err) {
            return res.serverError(err);
          }

          delete foundUser.__old;
          res.ok(foundUser.profileSanitize());
        });
      });
  },

  uploadAccess : function (req, res) {
    var params = [];
    var signature;
    var signedKey;

    params.push("format=" + req.body.format);
    params.push("tags=" + req.body.tags)
    params.push("timestamp=" + req.body.timestamp);
    params.push("type=upload" + sails.config.cloudinary.apiSecret);
    signature = params.join("&");
    signedKey = cryptojs.SHA1(signature).toString(cryptojs.enc.Hex);
    res.ok({
      cloud_name : sails.config.cloudinary.cloudName,
      api_key : sails.config.cloudinary.apiKey,
      signature : signedKey
    });
  },

  logout : function (req, res) {
    Sessions.findOne({
      accessToken : req.token
    }).exec(function (err, session) {
      if (err) {
        return res.serverError(err);
      } else if (!session) {
        return res.badRequest();
      }

      session.isDeleted = true;
      session.save(function (err) {
        if (err) {
          return res.Server-siderror(err);
        }

        res.json("Logout Successful.");
      });
    });
  },

  zcommerceNotifications : function (req, res) {
    async.parallel({
      shop : function (parallelCb) {
        Shops.findOne({
          userId : req.userId,
          isDeleted : false
        }).exec(function (err, shop) {
          if (err) {
            return parallelCb(err);
          } else if (!shop) {
            return parallelCb("error");
          }

          Orders.count({
            shopId : shop.id,
            isCompleted : true,
            appType : req.appType,
            isDeleted : false
          }).exec(function (err, count) {
            if (err) {
              return parallelCb(err);
            }

            parallelCb(null, count);
          });
        });
      },

      cart : function (parallelCb) {
        Cart.count({
          userId : req.userId,
          status : "added",
          appType : req.appType,
          isDeleted : false
        }).exec(function (err, count) {
          if (err) {
            return parallelCb(err);
          }

          parallelCb(null, count);
        });
      },

      messages : function (parallelCb) {
        Notifications.count({
          entityType : "message",
          notifiedId : req.userId,
          status : "unread",
          appType : req.appType,
          isDeleted : false
        }).exec(function (err, count) {
          if (err) {
            return parallelCb(err);
          }

          parallelCb(null, count);
        });
      }
    }, function (err, result) {
      if (err) {
        if (res.hasOwnProperty(err.type)) {
          return res[err.type](err.msg);
        }

        return res.serverError(err);
      }

      res.ok(result);
    });
  },

  me : function (req, res) {
    var query = {
      id : req.userId,
      isDeleted : false
    };

    Users.findOne(query)
      .exec(function (err, foundUser) {
        if (err) {
          return res.serverError(err);
        } else if (!foundUser) {
          return res.notFound("User not found.");
        }

        foundUser.checkBlocked({
          userId : req.userId,
          appType : req.appType
        }, function (err) {
          if (err) {
            if (res.hasOwnProperty(err.type)) {
              return res[err.type](err.msg);
            }

            return res.serverError(err);
          }

          if (foundUser.id === req.userId) {
            foundUser.__self = true;
          }

          res.ok(foundUser.sanitize());
        });
    });
  },

  resetPassword : function (req, res) {
    var resetCode = CryptoService.generateToken();
    var redirectUrl = sails.config.zcommerce.forgotPasswordRedirect
    if (req.userId) {
      return res.badRequest();
    }

    Users.findOne({
      email : req.body.email
    }).exec(function (err, user) {
      if (err) {
        return res.serverError(err);
      } else if (!user || !user.email) {
        sails.log.verbose("Someone wanted to reset other people's password.");
        return res.ok({
          status : "success",
          msg : "Email sent."
        });
      }

      user.resetPasswordToken = resetCode;
      user.__old = user.toObject();
      user.save(function (err) {
        if (err) {
          return res.serverError(err);
        }

        var mailOptions = {
          to : user.email,
          from : sails.config.zcommerce.mailer,
          fromName : sails.config.zcommerce.appName,
          subject :  'Your Bentanayan password has been reset.',
          html : '<a href="' + redirectUrl + '/resetpassword?rt=' + resetCode + '">Click here</a> to reset your password.'
        };

        EmailService.sendMail(mailOptions, function (err, result) {
          if (err) {
            sails.log.error(err);
            return res.serverError(err);
          }

          sails.log.verbose(result);
          res.ok({
            status : "success",
            msg : "Email sent."
          });
        });
      });
    });
  },

  setPassword : function (req, res) {
    Users.findOneByResetPasswordToken(req.body.token)
      .exec(function (err, user) {
        if (err || !user) {
          return res.serverError(err);
        }

        user.__old = user.toObject();
        user.password = req.body.__p;
        user.confirmPassword = req.body.__p;
        user.resetPasswordToken = "";
        user.save(function (err) {
          if (err) {
            return res.serverError(err);
          }

          var mailOptions = {
            to : user.email,
            from : sails.config.zcommerce.mailer,
            fromName : sails.config.zcommerce.appName,
            subject :  "You've successfully reset your password.",
            html : "<h4>Congratulations!</h4>"
          };

          EmailService.sendMail(mailOptions, function (err, result) {
            if (err) {
              sails.log.error(err);
              return res.serverError(err);
            }

            sails.log.verbose(result);
            res.ok({
              status : "success"
            });
          });
        });
      });
  },

  verifyToken : function (req, res) {
    switch (req.body.type) {
      case "resetPasswordToken":
        Users.findOneByResetPasswordToken(req.body.token)
          .exec(function (err, user) {
            if (err) {
              return res.serverError(err);
            } else if (!user) {
              return res.badRequest();
            }

            res.ok({
              status : "valid"
            });
          });
        return;
      default:
        res.badRequest();
    }
  }
};

