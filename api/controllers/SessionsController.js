/**
 * SessionsController
 *
 * @description :: Server-side logic for managing sessions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var requestHttp = require("request");
var underscore_string = require("underscore.string");

module.exports = {
  create : function(req, res, next) {
    var thisObj = this;
    var userEmail = req.body.userEmail;
    var password = req.body.password;
    var type = req.body.type;
    var refreshToken = req.body.refreshToken;
    var accessToken = req.body.accessToken;
    var socialNetwork = req.body.socialNetwork;
    //
    // native login
    //
    if (userEmail && password && type === "native") {
      return this._nativeSession(req, userEmail, password, function (err, sessionPayload) {
        if (err) {
          if (res.hasOwnProperty(err.type)) {
            return res[err.type](err.msg);
          }

          return res.serverError(err);
        }

        res.ok(sessionPayload);
      });
    } else if (userEmail && password && type === "ups") {
      return this._upsSession(req, userEmail, password, function (err, sessionPayload) {
        if (err) {
          if (res.hasOwnProperty(err.type)) {
            return res[err.type](err.msg);
          }

          return res.serverError(err);
        }

        res.ok(sessionPayload);
      });
    //
    // refreshing user access token
    //
    } else if (refreshToken && accessToken) {
      return this._regenerateSession(req, {
        requestToken : requestToken,
        accessToken : accessToken
      }, function (err, sessionPayload) {
        if (err) {
          if (res.hasOwnProperty(err.type)) {
            return res[err.type](err.msg);
          }

          return res.serverError(err);
        }

        res.ok(sessionPayload);
      });
    //
    // social network login
    //
    } else if (accessToken && socialNetwork) {
      switch(socialNetwork) {
        case "facebook":
        case "fb":
          return this._fbSession(req, accessToken, function (err, sessionPayload) {
            if (err) {
              if (res.hasOwnProperty(err.type)) {
                return res[err.type](err.msg);
              }

              return res.serverError(err);
            }

            res.ok(sessionPayload);
          });
        break;
        case "twitter":
          return this._twitterSession(req, accessToken, function (err, sessionPayload) {
            if (err) {
              if (res.hasOwnProperty(err.type)) {
                return res[err.type](err.msg);
              }

              return res.serverError(err);
            }
          });
        break;
      }
    }

    res.forbidden("Missing login credentials.");
  },

  _nativeSession : function (req, userEmail, password, callback) {
    var self = this;

    sails.log.verbose("Starting native session.");

    userEmail = userEmail.trim();
    password = password.trim();

    Users.findOne({
      where : {
        or : [
          {email : userEmail},
          {username : userEmail}
        ]
      }
    }).exec(function(err, user) {
      if (err) {
        return callback(err);
      } else if (!user) {
        return callback({
          type : "forbidden",
          msg : "Invalid email or password."
        });
      }

      Users.verifyPassword(password, user.password, function(err, isMatch) {
        if (err) {
          return callback(err);
        } else if (!isMatch) {
          return callback({
            type : "forbidden",
            msg : "Invalid email or password."
          });
        }

        self._generateSession(req, user.id, function(err, sessionPayload) {
          if (err) {
            return callback(err);
          }

          callback(null, sessionPayload);
        });
      });
    });
  },

  _upsSession : function (req, userEmail, password, callback) {
    var self = this;

    async.waterfall([
      function (waterfallCb) {
        requestHttp.post({
          url : sails.config.ecash.loginUrl,
          formData : {
            api_token : sails.config.ecash.apiToken,
            api_key : sails.config.ecash.apiKey,
            username : userEmail,
            password : password
          }
        }, function (err, result, body) {
          if (err) {
            return waterfallCb(err);
          }

          try {
            body = JSON.parse(body);
          } catch (e) {
            sails.log.error("SessionsController :: _upsSession ::", e.message);
            return waterfallCb("Error.");
          }

          waterfallCb(null, body);
        });
      },
      function (eCashUser, waterfallCb) {
        Users.findOneByUsername(userEmail)
          .exec(function (err, user) {
            if (err) {
              return waterfallCb(err);
            } else if (user) {
              return waterfallCb(null, user.id);
            }

            var userObj = null;

            try {
              userObj = {
                // email : userEmail + "@globalpinoyremittance.com",
                username : userEmail,
                password : password,
                firstName : eCashUser.merchant_name,
                metadata : {},
                registrationType : "upsexpress",
                appOrigin : req.appType
              };

              userObj.metadata[req.appType] = {
                username : userEmail,
                merchantId : eCashUser.merchant_id
              };
            } catch (e) {
              sails.log.error(e);
              return waterfallCb({
                type : "serverError",
                msg : ""
              });
            }

            Users.create(userObj).exec(function (err, user) {
              if (err) {
                return waterfallCb(err);
              }

              waterfallCb(null, user.id);
            });
          });
      },
      function (userId, waterfallCb) {
        self._generateSession(req, userId, function (err, sessionPayload) {
          if (err) {
            return waterfallCb(err);
          }

          waterfallCb(null, sessionPayload);
        });
      }
    ], function (err, result) {
      if (err) {
        return callback(err);
      }

      callback(null, result);
    });
  },

  _fbSession : function (req, accessToken, callback) {
    var self = this;

    sails.log.verbose("Starting facebook session.");

    async.waterfall([
      function (waterfallCb) {
        requestHttp({
          url : sails.config.fbProfileUrl + accessToken
        }, function (err, result, body) {
          if (err) {
            return waterfallCb(err);
          }

          try {
            body = JSON.parse(body);

            if (body.error) {
              throw new Error(body.error);
            }
          } catch (e) {
            sails.log.error(e);
            return waterfallCb({
              type : "serverError",
              msg : ""
            });
          }

          sails.log.verbose(body);
          waterfallCb(null, body);
        });
      },
      function (fbUser, waterfallCb) {
        Users.findOneByEmail(fbUser.email)
          .exec(function (err, foundUser) {
          if (err) {
            return waterfallCb(err);
          } else if (foundUser) {
            return waterfallCb(null, foundUser.id);
          }

          var userObj = null;

          try {
            userObj = {
              fbId : fbUser.id,
              email : fbUser.email,
              firstName : fbUser.first_name,
              middleName : fbUser.middle_name,
              lastName : fbUser.last_name,
              birthdate : fbUser.birthday,
              gender : fbUser.gender,
              profilePhoto : {
                is_silhouette : fbUser.picture.data.is_silhouette,
                secure_url : fbUser.picture.data.url,
                source : "facebook"
              },
              registrationType : "facebook",
              appOrigin : req.appType
            };
          } catch (e) {
            sails.log.error(e);
            return waterfallCb({
              type : "serverError",
              msg : ""
            });
          }

          Users.create(userObj).exec(function (err, user) {
            if (err) {
              return waterfallCb(err);
            }

            waterfallCb(null, user.id);
          });
        });
      },
      function (userId, waterfallCb) {
        self._generateSession(req, userId, function (err, sessionPayload) {
          if (err) {
            return waterfallCb(err);
          }

          waterfallCb(null, sessionPayload);
        });
      }
    ], function (err, waterfallResult) {
      if (err) {
        return callback(err);
      }

      callback(null, waterfallResult);
    });
  },

  _twitterSession : function (req, accessToken, callback) {
  },

  _regenerateSession : function (req, tokens, callback) {
    var self = this;

    sails.log.verbose("Starting session regeneration.");

    if (!Utils.checkProps(tokens, ["accessToken, requestToken"])) {
      return callback({
        type : "serverError",
        msg : ""
      });
    }

    var refreshToken = token.refreshToken.trim();
    var accessToken = token.accessToken.trim();

    Sessions.findOne({
      accessToken : accessToken,
      refreshToken : refreshToken,
      expiresAt : {
        ">=" : new Date()
      },
      isValid : true
    }).exec(function (err, session) {
      if (err) {
        return callback(err);
      } else if (!session) {
        return callback({
          type : "notFound",
          msg : "Session not found."
        });
      }

      self._generateSession(req, session.userId, function (err, sessionPayload) {
        if (err) {
          return callback(err);
        }

        Sessions.update({
          id : session.id
        }, {
          isValid : false
        }).exec(function (err, resultSession) {
          if (err) {
            return callback(err);
          }

          callback(null, sessionPayload);
        });
      });
    });
  },

  _generateSession : function(req, user, callback) {
    if (!user) {
      sails.log.error("Unable to generate new session, user is null or undefined");
      return callback({
        type : "serverError",
        msg : ""
      });
    }

    var metadata = req.headers;
    // metadata.ip = req.ip;

    Sessions.create({
      userId : user,
      refreshToken : CryptoService.generateToken(),
      accessToken : CryptoService.generateToken(),
      appType : req.appType,
      metadata : metadata
    }).exec(function (err, session) {
      if (err) {
        return callback(err);
      }

      session.loginPayload({
        appType : req.appType,
        userId : user
      }, function (err, payload) {
        if (err) {
          return callback(err);
        }

        callback(null, payload);
      });
    });
  },

  twitter : function (req, res) {
    requestHttp.post({
      url : sails.config.twitter.requestTokenUrl,
      oauth : {
        consumer_key : sails.config.twitter.consumerKey,
        consumer_secret : sails.config.twitter.consumerSecret,
        callback : sails.config.twitter.callbackUrl
      }
    }, function (err, result, body) {
      if (err) {
        return res.serverError(err);
      }

      var parsedBody = Utils.queryStringToJSON(body);

      TwitterAuth.create({
        token : parsedBody.oauth_token,
        secret : parsedBody.oauth_token_secret
      }).exec(function (err, result) {
        if (err) {
          return res.serverError(err);
        }

        res.ok({
          oauth_data : body
        });
      });
    });
  },

  twitteroAuthResponse : function (req, res) {
    var self = this;

    async.waterfall([
      function (callback) {
        TwitterAuth.update({
          token : req.query.token,
          isDeleted : false,
          createdAt : {
            ">=" : new Date(Date.now() - sails.config.twitter.twitterAuthExpire)
          }
        }, {
          isDeleted : true
        }).exec(function (err, auth) {
          if (err) {
            return callback(err);
          }

          callback();
        });
      },
      function (callback) {
        requestHttp.post({
          url : "https://api.twitter.com/oauth/access_token",
          oauth : {
            consumer_key: sails.config.twitter.consumerKey,
            consumer_secret: sails.config.twitter.consumerSecret,
            token : req.query.token,
            verifier : req.query.verifier,
          }
        }, function (err, result, body) {
          if (err) {
            return callback(err);
          }

          var bodyJSON = null;

          try {
            bodyJSON = Utils.queryStringToJSON(body);
          } catch (e) {
            sails.log.error(e);
            return callback({
              type : "serverError",
              msg : ""
            });
          }

          callback(null, bodyJSON);
        });
      },
      function (bodyJSON, callback) {
        requestHttp.get({
          url : "https://api.twitter.com/1.1/account/verify_credentials.json",
          oauth : {
            consumer_key: sails.config.twitter.consumerKey,
            consumer_secret: sails.config.twitter.consumerSecret,
            token : bodyJSON.oauth_token,
            token_secret : bodyJSON.oauth_token_secret,
          },
          qs : {
            include_entities : "false",
            skip_status: "true",
            include_email : "true"
          },
          json : true
        }, function (err, result, twitterUser) {
          if (err) {
            return callback(err);
          }

          callback(null, twitterUser);
        });
      },
      function (twitterUser, callback) {
        var userObj = {};
        var name = null;
        try {
          sails.log.verbose("Twitter User:", twitterUser);

          if (!twitterUser) {
            throw new Error("Twitter user unedefined.");
          }

          name = twitterUser.name.split(" ");

          userObj.twitterId = twitterUser.id_str;
          userObj.email = twitterUser.email;
          userObj.firstName = name[0] + (name[2] ? (" " + name[1]) : "");
          userObj.lastName = (name[2] ? name[2] : name[1]);
          userObj.profilePhoto = {
            secure_url : twitterUser.profile_image_url_https,
            source : "twitter"
          };
          userObj.registrationType = "twitter";
          userObj.appOrigin = req.appType;

        } catch (e) {
          sails.log.error(e);
          return callback({
            type : "serverError",
            msg : ""
          });
        }

        Users.findOneByEmail(userObj.email)
          .exec(function (err, foundUser) {
            if (err) {
              return callback(err);
            } else if (foundUser) {
              return callback(null, foundUser.id);
            }

            Users.create(userObj)
              .exec(function (err, createdUser) {
                if (err) {
                  return callback(err);
                }

                callback(null, createdUser.id);
              });
          })
      },
      function (userId, callback) {
        self._generateSession(req, userId, function (err, sessionPayload) {
          if (err) {
            return callback(err);
          }

          callback(null, sessionPayload);
        });
      }
    ], function (err, waterfallResult) {
      if (err) {
        if (res.hasOwnProperty(err.type)) {
          return res[err.type](err.msg);
        }

        return res.serverError(err);
      }

      res.ok(waterfallResult);
    });
  }
};
