/**
 * RoomsController
 *
 * @description :: Server-side logic for managing rooms
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  find : function (req, res) {
    if (!req.query.limit || req.query.limit > 100 || req.query.limit < 0) {
      req.query.limit = 100;
    }

    try {
      if (req.query.where) {
        req.query.where = JSON.parse(req.query.where);
      } else {
        req.query.where = {};
      }
    } catch (e) {
      sails.log.error(e);
      return req.badRequest("Malformed query.");
    }

    _.extend(req.query.where, {
      users : req.userId,
      appType : req.appType,
      isDeleted : false
    });

    async.waterfall([
      function (waterfallCb) {
        Rooms.find(req.query)
          .exec(function (err, rooms) {
            if (err) {
              return waterfallCb(err);
            }

            waterfallCb(null, rooms);
          });
      },
      function (rooms, waterfallCb) {
        async.map(rooms, function (room, mapCb) {
          async.parallel([
            function (parallelCb) {
              if (room.users[0] === "self") {
                return process.nextTick(parallelCb);
              }

              Users.findOneById(room.users[0])
                .exec(function (err, user) {
                  if (err) {
                    return parallelCb(err);
                  }

                  user = user.profileSanitize();
                  if (user.id === req.userId) {
                    user.__self = true;
                  }

                  parallelCb(null, user);
                });
            },
            function (parallelCb) {
              if (room.users[1] === "self") {
                return process.nextTick(parallelCb);
              }

              Users.findOneById(room.users[1])
                .exec(function (err, user) {
                  if (err) {
                    return parallelCb(err);
                  }

                  user = user.profileSanitize();
                  if (user.id === req.userId) {
                    user.__self = true;
                  }

                  parallelCb(null, user);
                });
            },
            function (parallelCb) {
              Messages.find({
                roomId : room.id,
                isDeleted : false
              }).limit(50)
                .sort({createdAt : -1})
                .exec(function (err, messages) {
                  if (err) {
                    return parallelCb(err);
                  }

                  messages = _.map(messages, function (m) {
                    return m.sanitize();
                  });

                  _.reverse(messages);
                  parallelCb(null, messages);
                });
            }
          ], function (err, result) {
            if (err) {
              return mapCb(err);
            }

            room.messages = result.splice(2,1)[0];
            room.users = result;
            mapCb(null, room);
          });
        }, function (err, mapResult) {
          if (err) {
            return waterfallCb(err);
          }

          waterfallCb(null, mapResult);
        });
      }
    ], function (err, result) {
      if (err) {
        if (res.hasOwnProperty(err.type)) {
          return res[err.type](err.msg);
        }

        return res.serverError(err);
      }

      res.ok(result);
    });
  },

  findOne : function (req, res) {
    if (!req.query.limit || req.query.limit > 100 || req.query.limit < 0) {
      req.query.limit = 100;
    }

    async.waterfall([
      function (waterfallCb) {
        Rooms.findOne({
          id : req.params.id,
          users : req.userId,
          appType : req.appType,
          isDeleted : false
        }).exec(function (err, room) {
          if (err) {
            return waterfallCb(err);
          } else if (!room) {
            return waterfallCb({
              type : "notFound",
              msg : "Room not found."
            });
          }

          waterfallCb(null, room.sanitize());
        });
      },
      function (room, waterfallCb) {
        async.parallel([
          function (parallelCb) {
            if (room.users[0] === "self") {
              return process.nextTick(parallelCb);
            }

            Users.findOneById(room.users[0])
              .exec(function (err, user) {
                if (err) {
                  return parallelCb(err);
                }

                user = user.sanitize();
                if (user.id === req.userId) {
                  user.__self = true;
                }

                parallelCb(null, user);
              });
          },
          function (parallelCb) {
            if (room.users[1] === "self") {
              return process.nextTick(parallelCb);
            }

            Users.findOneById(room.users[1])
              .exec(function (err, user) {
                if (err) {
                  return parallelCb(err);
                }

                user = user.sanitize();
                if (user.id === req.userId) {
                  user.__self = true;
                }

                parallelCb(null, user);
              });
          }
        ], function (err, result) {
          if (err) {
            return waterfallCb(err);
          }

          room.users = result;
          waterfallCb(null, room);
        });
      },
      function (room, waterfallCb) {
        Messages.find({
          roomId : room.id,
          isDeleted : false
        }).skip(req.query.skip)
          .limit(req.query.limit)
          .sort({createdAt : -1})
          .exec(function (err, messages) {
            if (err) {
              return waterfallCb(err);
            }

            room.messages = _.map(messages, function (m) {
              return m.sanitize();
            });
            waterfallCb(null, room);
          });
      }
    ], function (err, result) {
      if (err) {
        if (res.hasOwnProperty(err.type)) {
          return res[err.type](err.msg);
        }

        return res.serverError(err);
      }

      res.ok(result);
    });
  },

  markRead : function (req, res) {
    Rooms.findOne({
      id : req.params.id,
      appType : req.appType,
      isDeleted : false
    }).exec(function (err, room) {
      if (err) {
        return res.serverError(err);
      } else if (!room) {
        return res.notFound("Room not found.");
      }

      room.unreadCount = 0;
      room.save(function (err) {
        if (err) {
          return res.serverError(err);
        }

        Notifications.update({
          entity : req.params.id,
          entityType : "message",
          appType : req.appType,
          isDeleted : false
        }, {
          status : "read"
        }).exec(function (err) {
          if (err) {
            return res.serverError(err);
          }

          res.json("Success.");
        });
      });
    });
  }
};

