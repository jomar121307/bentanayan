/**
 * ProductsController
 *
 * @description :: Server-side logic for managing products
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var ObjectID = require("objectid");

module.exports = {
  create : function (req, res) {
    try {
      req.body = updateVerifier(Products, req.body);
    } catch (e) {
      return res.badRequest(e.message);
    }

    req.body.userId = req.userId;
    req.body.appType = req.appType;

    Shops.findOne({
      userId : req.userId,
      isDeleted : false
    }).exec(function (err, foundShop) {
      if (err) {
        return res.serverError(err);
      } else if (!foundShop) {
        return res.badRequest("Shop not found.");
      }

      req.body.shopId = foundShop.id;
      Products.create(req.body)
        .exec(function (err, createdProduct) {
          if (err) {
            return res.serverError(err);
          }

          res.ok(createdProduct.sanitize(req.appType));
        });
    });
  },

  find : function (req, res) {
    if (!req.query.limit || req.query.limit > 100 || req.query.limit < 0) {
      req.query.limit = 100;
    }

    if (req.query.random === "true") {
      if (req.query.limit) {
        req.query.limit = parseInt(req.query.limit);
      }

      if (req.query.skip) {
        req.query.skip = parseInt(req.query.skip);
      }

      return Products.findRandom({
        appType : req.appType,
        where : req.query.where,
        skip : req.query.skip,
        limit : req.query.limit,
        sort : req.query.sort
      }, function (err, payload) {
        if (err) {
          if (res.hasOwnProperty(err.type)) {
            return res[err.type](err.msg);
          }

          return res.serverError(err);
        }

        payload.products = _.map(payload.products, function (product) {
          product.userId = product.userId.sanitize();
          return product.sanitize();
        });

        res.ok(payload);
      });
    }

    try {
      if (req.query.where) {
        req.query.where = JSON.parse(req.query.where);
      } else {
        req.query.where = {};
      }
    } catch (e) {
      sails.log.error(e);
      return res.badRequest("Malformed query.");
    }

    _.extend(req.query.where, {
      isDeleted : false,
      appType : req.appType
    });

    async.waterfall([
      function (waterfallCb) {
        Products.count()
          .where(req.query.where)
          .exec(function (err, count) {
            if (err) {
              return waterfallCb(err);
            }

            waterfallCb(null, {
              count : count,
              products : []
            });
          });
      },
      function (payload, waterfallCb) {
        if (!payload.count) {
          return process.nextTick(function () {
            waterfallCb(null, payload);
          });
        }

        Products.find(req.query)
          .populateAll()
          .exec(function (err, foundProducts) {
            if (err) {
              return waterfallCb(err);
            }

            foundProducts = _.map(foundProducts, function (product) {
              product.userId = product.userId.sanitize();
              product.shopId = product.shopId.sanitize(req.appType);
              product.currencyId = product.currencyId.sanitize();

              return product.sanitize();
            });
            payload.products = foundProducts;
            waterfallCb(null, payload);
          });
        }
    ], function (err, waterfallResult) {
      if (err) {
        if (res.hasOwnProperty(err.type)) {
          return res[err.type](err.msg);
        }

        return res.serverError(err);
      }

      res.ok(waterfallResult);
    });
  },

  findOne : function (req ,res) {
    if (!req.query.limit || req.query.limit > 100 || req.query.limit < 0) {
      req.query.limit = 100;
    }

    async.waterfall([
      //
      // Find identity of requester (if possible);
      //
      function (waterfallCb) {
        var token = req.headers.authorization;
        token = !!token && token.indexOf("Bearer ") === 0 && token.replace("Bearer ", "");

        if (!token || token === -1) {
          return process.nextTick(function () {
            waterfallCb(null, false);
          });
        }

        Sessions.findOne({
          appType : req.appType,
          accessToken : token
        }).exec(function (err, session) {
          if (err) {
            return waterfallCb(err);
          } else if (!session) {
            sails.log.info("ProductsController :: findOne :: Invalid token.");
            return waterfallCb(null, false);
          }

          waterfallCb(null, session.userId);
        });
      },
      function (userId, waterfallCb) {
        var query = {};

        if (ObjectID.isValid(req.params.id)) {
          query.id = req.params.id;
        } else {
          query.name = req.params.id;
        }

        _.extend(query, {
          isDeleted : false,
          appType : req.appType
        });

        Products.findOne(query)
          .populateAll()
          .exec(function (err, foundProduct) {
            if (err) {
              return waterfallCb(err);
            } else if (!foundProduct) {
              return waterfallCb({
                type : "notFound",
                msg : "Product not found."
              });
            } else if (foundProduct.userId.id === userId) {
              foundProduct.__self = true;
            }

            waterfallCb(null, foundProduct);
          });
      },
      function (foundProduct, waterfallCb) {
        if (req.query.reviews !== "true") {
          foundProduct = foundProduct.sanitize();

          return process.nextTick(function () {
            waterfallCb(null, foundProduct);
          });
        } else if (req.query.reviews === "true" && !foundProduct.reviewCount) {
          foundProduct = foundProduct.sanitize();
          foundProduct.reviews = [];

          return process.nextTick(function () {
            waterfallCb(null, foundProduct);
          });
        }

        foundProduct.fetchReviews({
          appType : req.appType,
          limit : req.query.limit,
          skip : req.query.skip,
          sort : req.query.sort
        }, function (err) {
          if (err) {
            return waterfallCb(err);
          }

          waterfallCb(null, foundProduct.sanitize());
        });
      }
    ], function (err, waterfallResult) {
      if (err) {
        if (res.hasOwnProperty(err.type)) {
          return res[err.type](err.msg);
        }

        return res.serverError(err);
      }

      res.ok(waterfallResult);
    });
  },

  update : function (req, res) {
    try {
      req.body = updateVerifier(Products, req.body);
    } catch (e) {
      return res.badRequest(e.message);
    }

    Products.findOne({
      id : req.params.id,
      userId : req.userId,
      appType : req.appType,
      isDeleted : false
    }).exec(function (err, foundProduct) {
      if (err) {
        return res.serverError(err);
      } else if (!foundProduct) {
        return res.notFound("Product not found.");
      }

      foundProduct.__old = foundProduct.toObject();
      _.extend(foundProduct, req.body);

      foundProduct.save(function (err) {
        if (err) {
          return res.serverError(err);
        }

        delete foundProduct.__old;
        res.ok(foundProduct.sanitize());
      });
    });
  },

  destroy : function (req, res) {
    Products.findOne({
      id : req.params.id,
      userId : req.userId,
      appType : req.appType,
      isDeleted : false
    }).exec(function (err, foundProduct) {
      if (err) {
        return res.serverError(err);
      } else if (!foundProduct) {
        return res.notFound("Product not found.");
      }

      foundProduct.isDeleted = true;
      foundProduct.save(function (err) {
        if (err) {
          return res.serverError(err);
        }

        res.ok(foundProduct.sanitize());
      });
    });
  },

  resell : function (req, res) {
    async.waterfall([
      function (waterfallCb) {
        Shops.fetchPaypalInfo({
          userId : req.userId,
          appType : req.appType
        }, function (err, paypalInfo) {
          if (err) {
            return waterfallCb(err);
          }

          waterfallCb();
        });
      },
      function (waterfallCb) {
        Products.findOne({
          id : req.params.id,
          appType : req.appType,
          isDeleted : false
        }).exec(function (err, foundProduct) {
          if (err) {
            return waterfallCb(err);
          } else if (!foundProduct) {
            return waterfallCb({
              type : "notFound",
              msg : "Product not found."
            });
          } else if (foundProduct.userId === req.userId) {
            return waterfallCb({
              type : "badRequest",
              msg : "You own this product."
            });
          // if the product is resold, point to the original product.
          } else if (_.safe(foundProduct, "metadata.isResold")) {
            return Products.findOne({
              id : foundProduct.id,
              applied : req.appType,
              isDeleted : false
            }).exec(function (err, originalProduct) {
              if (err) {
                return waterfallCb(err);
              } else if (!originalProduct) {
                sails.log.error("ProductsController :: resell :: Error. Orphaned product");
                return waterfallCb({
                  type : "serverError",
                  msg : ""
                });
              } else if (!_.safe(originalProduct, "metadata.isResold")) {
                sails.log.error("ProductsController :: resell :: Error. Parent product can't be resold.");
                return waterfallCb({
                  type : "serverError",
                  msg : ""
                });
              }

              waterfallCb(null, originalProduct);
            });
          }

          waterfallCb(null, foundProduct);
        });
      },
      function (foundProduct, waterfallCb) {
        foundProduct.checkResellStatus(function (err) {
          waterfallCb(err, foundProduct);
        });
      },
      function (foundProduct, waterfallCb) {
        var newProduct = foundProduct.toObject();

        newProduct.userId = req.userId;
        newProduct.shopId = req.shopId;
        newProduct.metadata = {
          sourceShop : foundProduct.shopId,
          sourceProduct : foundProduct.id,
          isResold : true,
        };
        newProduct.randomPoint = [Math.random(), Math.random()];

        Products.create(newProduct)
          .exec(function (err, createdProduct) {
            if (err) {
              return waterfallCb(err);
            }

            waterfallCb(null, foundProduct, createdProduct.sanitize());
          });
      },
      function (foundProduct, createdProduct, waterfallCb) {
        try {
          // skip saving if product has already the hasClones flag.
          if (foundProduct.metadata.hasClones) {
            return waterfallCb(null, createdProduct);
          }

          foundProduct.metadata.hasClones = true;
        } catch (e) {
          sails.log.error(e);
          sails.log.error("ProductsController :: resell :: To be resold product has no metadata.");
          return waterfallCb({
            type : "serverError",
            msg : ""
          });
        }

        foundProduct.save(function (err) {
          if (err) {
            return waterfallCb(err);
          }

          waterfallCb(null, createdProduct);
        });
      }
    ], function (err, waterfallResult) {
      if (err) {
        if (res.hasOwnProperty(err.type)) {
          return res[err.type](err.msg);
        }

        res.ok(waterfallResult);
      }
    });
  },

  checkName : function (req, res) {
    Products.findOne({
      userId : req.userId,
      name : req.query.name,
      appType : req.appType,
      isDeleted : false
    }).exec(function (err, product) {
      if (err) {
        return res.serverError(err);
      } else if (product && req.query.id !== product.id) {
        return res.json("Taken");
      }

      res.json("Available");
    });
  }
};
