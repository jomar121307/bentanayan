/**
 * MessagesController
 *
 * @description :: Server-side logic for managing Messages
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  announce : function(req, res) {
    Users.findOneById(req.userId)
      .exec(function (err, user) {
        if (err) {
          return res.serverError(err);
        } else if (!user) {
          return res.notFound("User not found.");
        }

        req.session.users = req.session.users || {};
        req.session.users[req.socket.id] = user;

        Users.subscribe(req, user, "message");
        res.json("Success");
      });
  },

  private : function (req, res) {
    Users.findOneById(req.session.users[req.socket.id].id)
      .exec(function (err, sender) {
        if (err) {
          return res.serverError(err);
        } else if (!sender) {
          return res.notFound("User not found.");
        }

        Messages.create({
          to : req.body.to,
          from : sender.id,
          payload : req.body.payload,
          appType : req.appType
        }).exec(function (err, message) {
          if (err) {
            return res.serverError(err);
          } else if (!message) {
            return res.badRequest();
          }

          Users.message(req.body.to, {
            from : sender.toJSON(),
            payload : req.body.payload
          });
        });
      });
  },

  initiate : function (req, res)  {
    if (!req.params.to) {
      return res.badRequest("Missing query parameter: to");
    }

    _.extend(req.body, {
      name : "private message",
      users : [req.userId, req.params.to],
      appType : req.appType,
      messageCount : 0,
      isDeleted : false
    });

    async.waterfall([
      function (waterfallCb) {
        var users = req.body.users;

        if (req.params.to === req.userId) {
          req.body.users = [req.userId, "self"];
          users = req.body.users;
        }

        Rooms.native(function (err, collection) {
          if (err) {
            return waterfallCb(err);
          }

          collection.findOne({
            users : {
              $all : users
            },
            appType : req.appType,
            isDeleted : false
          }, function (err, room) {
            if (err) {
              return waterfallCb(err);
            } else if (room) {
              return waterfallCb(null, room._id);
            }

            waterfallCb(null, false);
          });
        });
      },
      function (roomId, waterfallCb) {
        if (!!roomId) {
          return process.nextTick(function () {
            return waterfallCb(null, roomId);
          });
        }

        Rooms.create(req.body)
          .exec(function (err, room) {
            if (err) {
              return waterfallCb(err);
            } else if (!room) {
              sails.log.error("MessagesController :: initiate :: Room is undefined.");
              return waterfallCb("Error.");
            }

            waterfallCb(null, room.id);
          });
      },
      function (roomId, waterfallCb) {
        Messages.create({
          roomId : roomId + "",
          to : req.params.to,
          from : req.userId,
          payload : req.body.payload,
          appType : req.appType
        }).exec(function (err, message) {
          if (err) {
            return waterfallCb(err);
          } else if (!message) {
            sails.log.error("MessagesController :: initiate :: User is undefined.");
            return waterfallCb("Error.");
          }

          waterfallCb(null, message);
        });
      }
    ], function (err, result) {
      if (err) {
        if (res.hasOwnProperty(err.type)) {
          return res[err.type](err.msg);
        }

        return res.serverError(err);
      }

      res.ok(result);
    });
  },

  create : function(req, res, next) {
    _.extend(req.body, {
      roomId : req.body.roomId + "",
      to : req.params.to,
      from : req.userId,
      payload : req.body.payload,
      appType : req.appType
    });

    Messages.create(req.body)
      .exec(function (err, message) {
        if (err) {
          return res.serverError(err);
        } else if (!message) {
          sails.log.error("MessagesController :: create :: Message is undefined.");
          return res.serverError();
        }

        res.ok(message.sanitize());
      });
  },

	find : function (req, res, next) {
    if (!req.query.limit || req.query.limit > 100 || req.query.limit < 0) {
      req.query.limit = 100;
    }

    req.query.where = {
      from : req.userId,
      appType : req.appType,
      isDeleted : false
    }

    delete req.query.to;
    Messages.find(req.query)
      .populateAll()
      .exec(function (err, messages) {
      if (err) {
        return res.serverError(err);
      }

      messages = messages.map(function (msg) {
        msg.to = msg.to.sanitize();
        msg.from = msg.from.sanitize();

        return msg.sanitize();
      });

      res.ok(messages);
    });
  }
};

