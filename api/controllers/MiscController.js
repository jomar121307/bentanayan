/**
 * MiscController
 *
 * @description :: Server-side logic for managing Miscs
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var underscore_str = require("underscore.string");

module.exports = {
  create : function (req, res) {
    res.notFound();
  },

  find : function (req, res) {
    try {
      if (req.query.where) {
        req.query.where = JSON.parse(req.query.where);
      } else {
        req.query.where = {};
      }
    } catch (e) {
      sails.log.error(e);
      return res.badRequest("Malformed query.");
    }

    _.extend(req.query.where, {
      appType : req.appType
    });

    Misc.find(req.query)
      .exec(function (err, misc) {
        if (err) {
          return res.serverError(err);
        } else if (!misc.length) {
          return res.notFound();
        }

        misc = _.map(misc, function (m) {
          return m.sanitize();
        })

        res.ok(misc);
      });
  },

  findOne : function (req, res) {
    res.notFound();
  },

  update : function (req, res) {
    res.notFound();
  },

  destroy : function (req, res) {
    res.notFound();
  },

  addCategories : function (req, res) {
    if (typeof req.body.path !== "string") {
      return res.badRequest("Invalid body.");
    }

    Users.findOne({
      username : "bentanayanAdmin",
      isAdmin : true,
      appOrigin : req.appType
    }).exec(function (err, user) {
      if (err) {
        return res.serverError(err);
      } else if (!user) {
        sails.log.error("MiscController :: addCategories :: Admin account for", req.appType, "is missing.");
        return res.serverError("Error.");
      } else if (user.id !== req.userId) {
        sails.log.error("MiscController :: addCategories :: A Non-admin account tried to do funny things.");
        return res.serverError("Error.");
      }

      Misc.findOne({
        name : "Product Categories",
        appType : req.appType
      }).exec(function (err, categories) {
        if (err) {
          return res.serverError(err);
        } else if (!categories) {
          sails.log.error("MiscController :: addCategories :: Categories for", req.appType, "are missing.");
          return res.serverError("Error.");
        }

        var splat = _.compact(req.body.path.split("."));
        var slug = underscore_str.slugify(req.body.category.replace("'", ""))
        if (splat.length === 0) {
          if (_.findIndex(categories.metadata, {slug : slug}) !== -1) {
            return res.badRequest("Category already added.");
          }

          categories.metadata.push({
            name : req.body.category,
            slug : slug,
            subcategories : []
          });
        } else if (splat.length === 1) {
          var idx = _.findIndex(categories.metadata, {
            slug : splat[0]
          });
          var category = categories.metadata[idx];
          var subcategorygroup = null;

          if (idx === -1) {
            return res.badRequest("Category not found.");
          }

          subcategorygroup = _.filter(category.subcategories, function (sc) {
            return sc.groupheader.slug === slug;
          });

          if (subcategorygroup.length) {
            return res.badRequest("Subcategory group already added.");
          }

          _.ensure(category, "subcategories", []);
          category.subcategories.push({
            groupheader : {
              name : req.body.category,
              slug : slug
            },
            groupling : []
          });
        } else if (splat.length === 2) {
          var idx = _.findIndex(categories.metadata, {
            slug : splat[0]
          });
          var subcategories = [];
          var category = null;
          var subcategory = null;

          if (idx === -1) {
            return res.badRequest("Category not found.");
          }

          category = categories.metadata[idx];
          _.ensure(category, "subcategories", []);
          subcategory = _.filter(category.subcategories, function (sc) {
            return sc.groupheader.slug === splat[1];
          })[0];

          if (!subcategory) {
            return res.badRequest("Subcategory group not found.");
          } else if (_.findIndex(subcategory.groupling, {name : req.body.category}) !== -1) {
            return res.badRequest("Subcategory already added.");
          }

          _.ensure(subcategory, "groupling", []);
          subcategory.groupling.push({
            name : req.body.category,
            slug : slug
          });
        } else {
          return res.badRequest();
        }

        categories.save(function (err) {
          if (err) {
            return res.serverError(err);
          }

          res.ok(categories.sanitize());
        });
      });
    })
  },

  deleteCategories : function (req, res) {
    if (typeof req.body.path !== "string") {
      return res.badRequest("Invalid body.");
    }

    Users.findOne({
      username : "bentanayanAdmin",
      isAdmin : true,
      appOrigin : req.appType
    }).exec(function (err, user) {
      if (err) {
        return res.serverError(err);
      } else if (!user) {
        sails.log.error("MiscController :: addCategories :: Admin account for", req.appType, "is missing.");
        return res.serverError("Error.");
      } else if (user.id !== req.userId) {
        sails.log.error("MiscController :: addCategories :: A Non-admin account tried to do funny things.");
        return res.serverError("Error.");
      }

      Misc.findOne({
        name : "Product Categories",
        appType : req.appType
      }).exec(function (err, categories) {
        if (err) {
          return res.serverError(err);
        } else if (!categories) {
          sails.log.error("MiscController :: addCategories :: Categories for", req.appType, "are missing.");
          return res.serverError("Error.");
        }

        var splat = _.compact(req.body.path.split("."));
        var idx = _.findIndex(categories.metadata, {
          slug : splat[0]
        });
        var idx2 = -1;
        var idx3 = -1;

        if (idx === -1) {
          return res.badRequest("Category not found.");
        } else if (splat.length === 1) {
          categories.metadata.splice(idx, 1);
        } else if (splat.length === 2) {
          var category = categories.metadata[idx];;

          idx2 = _.findIndex(category.subcategories, function (sc) {
            return sc.groupheader.slug === splat[1];
          });

          if (idx2 === -1) {
            return res.badRequest("Subcategory group not found.");
          }

          category.subcategories.splice(idx2, 1);
        } else if (splat.length === 3) {
          var category = categories.metadata[idx];
          var subcategory = null;

          idx2 = _.findIndex(category.subcategories, function (sc) {
            return sc.groupheader.slug === splat[1];
          });

          if (idx2 === -1) {
            return res.badRequest("Subcategory group not found.");
          }

          subcategory = category.subcategories[idx2];
          idx3 = _.findIndex(subcategory.groupling, {
            slug : splat[2]
          });

          if (idx3 === -1) {
            return res.badRequest("Subcategory not found.")
          }

          subcategory.groupling.splice(idx3, 1);
        } else {
          return res.badRequest();
        }

        categories.save(function (err) {
          if (err) {
            return res.serverError(err);
          }

          res.ok(categories.sanitize());
        });
      });
    })
  }
};

