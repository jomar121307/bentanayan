# bentanayan-api

a [Sails](http://sailsjs.org) application

[How to get started with sailsjs](http://sailsjs.org/get-started)

*Caution* Must be in Linux Environment for smooth process of running locally

*To run sailsjs

1. Go to project's root
2. Type command npm install
3. Type sails lift
4. Voila!!! build/run should be successful


*For the email-service we have used [Sendgrid](https://sendgrid.com/)
To change the key go to /config/email.js


